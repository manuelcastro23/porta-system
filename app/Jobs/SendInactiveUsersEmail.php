<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\InactiveUserEmail;


class SendInactiveUsersEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            // Evaluacion de usuarios inactivos
            $inactiveUsers = User::where('last_login_at', '<=', now()->subDays(30))->get();
            
            foreach ($inactiveUsers as $user) {
                // Envio de Correos a usuarios Inactivos...
                $result =  Mail::to($user->email)->send(new InactiveUserEmail($user));
  
                if ($result) {
                    \Log::info("Correo electrónico enviado a {$user->email} correctamente.");
                } else {
                    \Log::error("Error al enviar correo electrónico a {$user->email}. {$result}");
                }
            }
            \Log::info('Se enviaron correos electrónicos a los usuarios inactivos correctamente.');
        } catch (\Exception $e) {
            \Log::error('Error al enviar correos electrónicos a los usuarios inactivos: ' . $e->getMessage());
        }
   
    }
}
